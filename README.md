[![RELEASE DATE](https://img.shields.io/github/release-date/VelheimRSPS/Velheim-RSPS-Launcher)](https://github.com/VelheimRSPS/Velheim-RSPS-Launcher/releases)
[![PULL REQUESTS](https://img.shields.io/github/issues-pr-raw/VelheimRSPS/Velheim-RSPS-Launcher)](https://github.com/VelheimRSPS/Velheim-RSPS-Launcher/blob/main/.github/ISSUES/feature_request.md)
[![BUG REPORTS](https://img.shields.io/github/issues-raw/VelheimRSPS/Velheim-RSPS-Launcher)](https://github.com/VelheimRSPS/Velheim-RSPS-Launcher/blob/main/.github/ISSUES/bug_report.md)
[![LAST COMMIT](https://img.shields.io/github/last-commit/VelheimRSPSVelheimRSPS/Velheim-RSPS-Launcher/main)](https://github.com/VelheimRSPS/Velheim-RSPS-Launcher)
[![TOTAL DOWNLOADS](https://img.shields.io/github/downloads/VelheimRSPS/Velheim-RSPS-Launcher/total)](https://github.com/VelheimRSPS/Velheim-RSPS-Launcher/releases/)
[![DISCORD SERVER](https://discordapp.com/api/guilds/402767531816910858/widget.png?style=shield)](https://discord.gg/bAtRnqb)

# Velheim RSPS Launcher
Windows Installer for Velheim RSPS

[![BANNER IMAGE](https://gitlab.com/Developer-Corner/development/Velheim-RSPS-Launcher/-/raw/main/app/logo.png)](https://www.velheim.com)

## Links
<a href="https://www.velheim.com/"> <img align="right" width="150" height="150" src="https://gitlab.com/Developer-Corner/development/Velheim-RSPS-Launcher/-/raw/main/app/icon.png"></a>

 - [Velheim RSPS Website](https://www.velheim.com)
 - [Velheim RSPS Forums](https://www.velheim.com/community)
 - [Velheim RSPS Discord](https://discord.gg/bAtRnqb)
